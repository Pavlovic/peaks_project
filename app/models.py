from app import db
from app import bcrypt
from flask_login import UserMixin
from sqlalchemy.ext.hybrid import hybrid_property

USER = 0
ADMIN = 1

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    user_login = db.Column(db.String(64), index=True, unique=True)
    _password = db.Column(db.String(120), index=True, unique=True)
    user_email = db.Column(db.String(120), index=True, unique=True)
    role = db.Column(db.SmallInteger, default=USER)
    tariff = db.Column(db.SmallInteger, default=1)
    accounts = db.relationship('Accounts', backref = 'owner', lazy = 'dynamic')
    
    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.id)
        
    @hybrid_property
    def password(self):
        return self._password
    
    @password.setter
    def _set_password(self, text):
        self._password = bcrypt.generate_password_hash(text)
        
    def is_correct_password(self, text):
        return bcrypt.check_password_hash(self._password, text)

class Accounts(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    resource = db.Column(db.String(120), index=True)
    app_id = db.Column(db.String(120), index=True)
    app_secret = db.Column(db.String(120), index=True)
    url = db.Column(db.String(120), index=True)
    token = db.Column(db.String(256), index=True)
    last_update = db.Column(db.String(64), default='2016-09-01 00:00:01')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))