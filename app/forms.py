from flask_wtf import Form
from wtforms import BooleanField, StringField, PasswordField, validators
from wtforms import TextField, BooleanField, PasswordField, IntegerField, FieldList, FormField
from wtforms.validators import Required

class LoginForm(Form):
    user_login = TextField('login', validators = [Required()])
    user_password = TextField('password', validators = [Required()])
    remember_me = BooleanField('remember_me', default = False)

class RegistrationForm(Form):
    user_login = StringField('Username', [validators.Length(min=4, max=64)])
    password = PasswordField('New Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    user_email = StringField('Email Address', [validators.Length(min=6, max=64)])
    resource = StringField('social network', [validators.Length(min=6, max=120)])
    url = StringField('url for quoting', [validators.Length(min=6, max=120)])
    token = StringField('long live token', [validators.Length(min=6, max=256)])

class MessageForm(Form):
    def __init__(self, csrf_enabled=False, *args, **kwargs):
        super(MessageForm, self).__init__(csrf_enabled=csrf_enabled, *args, **kwargs)
    m_id = StringField('m_id', validators = [Required()])
    sender = StringField('sender', validators = [Required()])
    text = StringField('text', validators = [Required()])
    class_mark = IntegerField('class_mark', validators = [Required()])
    correct_mark = IntegerField('correct_mark', validators = [Required()])

class MessagesForm(Form):
    title = StringField('title')
    msgs = FieldList(FormField(MessageForm))