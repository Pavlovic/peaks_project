from app import app
#from classifiers import load_model, update_classifier
from flask import jsonify
from flask import request
from flask import abort
from flask import render_template, flash, redirect
from flask import session, url_for, request, g
from flask_login import login_user, logout_user, current_user, login_required
from app.forms import LoginForm, RegistrationForm, MessageForm, MessagesForm
from app import db 
from app.models import User, Accounts
from sqlalchemy import exc
from pymongo import MongoClient
import pymongo

@app.route('/')
@app.route('/index')
def index():
    return render_template("index.html")

@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        print(form.user_login.data)
        user = User(user_login=form.user_login.data, password=form.password.data, 
                    user_email=form.user_email.data)
        try:
            db.session.add(user)
            db.session.commit()
        except exc.IntegrityError:
            return render_template("index.html")
        _user = User.query.filter_by(user_login=form.user_login.data).first()
        account = Accounts(resource = form.resource.data,
                           url = form.url.data,
                           token = form.token.data,
                           user_id = _user.id)
        try:
            db.session.add(account)
            db.session.commit()
        except exc.IntegrityError:
            return render_template("index.html")
        flash('Thanks for registering')
        return redirect(url_for('login'))
    return render_template('register.html', form=form)

@app.route('/login/',methods=['GET','POST'])
def login():
    if request.method == 'GET':
        return render_template('login2.html')
    username_ = request.form['username']
    password_ = request.form['password']
    remember_me = False
    if 'remember_me' in request.form:
        remember_me = True
    registered_user = User.query.filter_by(user_login=username_).first()
    if registered_user is None:
        print('here1')
        flash('Username or Password is invalid' , 'error')
        return redirect(url_for('login'))
    if registered_user.is_correct_password(password_):
        login_user(registered_user, remember = remember_me)
        flash('Logged in successfully')
        print('here2')
        return redirect(request.args.get('next') or url_for('index'))
    else:
        return redirect(url_for('login'))

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

@app.before_request
def before_request():
    g.user = current_user

@app.route('/personal_page/', methods=['GET','POST'])
#@login_required
def personal_page():
    """user = User.query.filter_by(user_login=nickname).first()
    if user == None:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('login'))
    user_id = user.id
    account = Accounts.query.filter_by(id=user_id).first()
    url = account.url"""
    mform = MessagesForm()
    mform.title.data = "INBOX" # change the field's data
    client = MongoClient()
    db = client.fb_messages
    cursor = db.fb_messages.find({"user_id": 1}).sort("class", -1)
    obj_ids = []
    for msg in cursor: # some database function to get a list of team members
        msg_form = MessageForm()
        msg_form.m_id = msg['id'] # These fields don't use 'data'
        msg_form.sender = msg['from']['name']
        msg_form.text = msg['message']
        msg_form.class_mark = msg['class']
        msg_form.correct_mark = ''
        mform.msgs.append_entry(msg_form)
        obj_ids.append((msg['_id'], msg['class']))
        print('here!!!!__')
    if request.method == 'POST':
        form = MessagesForm(request.form)
        for x, obj in zip(form.msgs.data, obj_ids):
            mark = x['class_mark']
            if mark != obj[1]:
                print('here!')
                db.fb_messages.update_one({'_id':obj[0]}, {'$inc':{'class':mark}})
    return render_template('test.html', mform = mform)

@app.route('/user/<nickname>')
@login_required
def user(nickname):
    user = User.query.filter_by(user_login=nickname).first()
    if user == None:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('index'))
    user_id = user.id
    client = MongoClient()
    db = client.fb_messages
    cursor = db.fb_messages.find({"user_id": user_id})
    recent_msg = [x['from']['name'] + ': ' + x['message'] for x in cursor]
    return render_template('user_page.html', user = user.user_login, messages = recent_msg)
    
@app.route('/get_access_token/<nickname>')
@login_required
def get_access_token(nickname):
    if request.method == 'GET':
        code = request.args.get('code')
        print(code)
    user = User.query.filter_by(user_login=nickname).first()
    if user != None:
        id_ = user.id
        Accounts=Accounts.query.filter_by(id=id_).first()
    return redirect(url_for('index'))

# API methods

@app.route('/api/v1.0/new_user', methods=['POST'])
def registration():
    if not request.json:
        abort(400)
    user = User(user_login = request.json['user_login'], 
               password = request.json['user_password'], 
               user_email = request.json['user_email'],
               tariff = request.json['tariff'])
    try:
        db.session.add(user)
        db.session.commit()
    except exc.IntegrityError:
        return jsonify({'status': 'user_already_exists'})
    return jsonify({'status': 'done'}), 201

@app.route('/api/v1.0/accounts_data', methods=['POST'])
def append_accounts():
    if not request.json:
        abort(400)
    user_login = request.json['user_login']
    user = User.query.filter_by(user_login=user_login).first()
    print(user.id)
    if user != None:
        password_ = request.json['user_password']
        if user.is_correct_password(password_):
            id = user.id
            account = Accounts(resource = request.json['resource'], 
                              app_id = request.json['app_id'], 
                              app_secret = request.json['app_secret'],
                              url = request.json['url'],
                              token = request.json['token'],
                              user_id = id)
    try:
        db.session.add(account)
        db.session.commit()
    except exc.IntegrityError:
        jsonify({'status': 'wrong data'})
    return jsonify({'status': 'done'}), 201