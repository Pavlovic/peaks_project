""" misc module """ 

from api_wrappers import fb
from app.models import Accounts
from sklearn import linear_model

def make_chunks():
    workers  = []
    chunk_list = []
    resources = ['facebook', 'vk']
    for x in resources:
        all_data = Accounts.query.filter_by(resource=x).all()
        chunk = [(uid.user_id, uid.url, uid.token, uid.last_update) for uid in all_data]
        print(chunk)
        if len(chunk) != 0:
            chunk_list.append(chunk)
    workers.append(fb)
    return chunk_list, workers

def validate_model(clf):
    if isinstance(clf, (linear_model.SGDClassifier, 
                        linear_model.PassiveAggressiveClassifier,
                        linear_model.Perceptron)):
        return True
    else:
        return False
