""" background web-crawler """

import json
import sqlite3
from api_wrappers import fb, vk
from misc import make_chunks
from multiprocessing import Process
from time import sleep

with open('C:\\flask\\config.json') as config:
    config = json.load(config)
    
path_to_db = config['pathdb']
path_to_storage = config['pathst']
bpfq = config['bpfq']
""" select user_id, resource, url, token """
while True:
    
    chunk_list, workers  = make_chunks(path_to_db)
    
    if __name__ == '__main__':
        processes = []
        for i in range(len(chunk_list)):
            p = Process(target=workers[i], args=(chunk_list[i], 
                                                path_to_storage,))
            p.start()
            processes.append(p)
            
        for p in processes:
            p.join()
    sleep(bpfq)