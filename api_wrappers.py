""" api wrappers module """

import pymongo
from app import db
from app.models import Accounts
from datetime import datetime
from misc import message_classifier, load_model
from pymongo import MongoClient
from facepy import utils
from facepy import GraphAPI
from sqlalchemy import exc


def fb(chunk, path_to_storage):
    client = MongoClient(path_to_storage)
    db = client.fb_messages
    
    for x in chunk:
        user_id = x[0]
        message_list = [] 
        graph = GraphAPI(x[2])
        args = {'fields' : 'message,from,created_time'}
        url_node = x[1]
        conv = graph.get(url_node)
        my_messages = url_node.split("/")[0]
        conv = [x for x in conv['data'] if x['updated_time'] > str(x[3])]
        try:
            session.query(Accounts).filter(url=url_node).update({'last_update':datetime.now()})
            session.commit()
        except exc.IntegrityError:
            print("db error")
        if len(conv) != 0:
            clf = load_model(my_message)
        for i in range(len(conv)):
            msg = graph.get(conv[i]['id'] + '/messages')
            for ms in msg['data']:
                content = graph.get(ms['id'], **args)
                if content['from']['id'] != my_messages:
                    content['user_id'] = user_id
                    text = content['message'] + " " + content['from']['name']
                    content['class'] = message_classifier(clf, text)
                    message_list.append(content)
        db.fb_messages.insert_many(message_list)
        # pushing to google doc 