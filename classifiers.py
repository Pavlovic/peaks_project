# ml_module
from os.path import exists, expanduser
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.linear_model import Perceptron
from sklearn.externals import joblib


def load_model(fb_id):
    classifier_dir = "~\\classifiers\\" + fb_id
    defaultpath = "~\\classifiers\\default\\default.pkl"
    if not os.path.exists(expanduser(classifier_dir)):
        os.makedirs(expanduser(classifier_dir))
        default_clf = joblib.load(expanduser(defaultpath))
        return default_clf
    else:
        classifier_dir = classifier_dir + "\\" + fb_id + ".pkl"
        classifier_dir = expanduser(classifier_dir)
    
    try:
        clf = joblib.load(classifier_dir)
    except FileNotFoundError:
        clf = joblib.load(expanduser(defaultpath))
    return clf

def message_classifier(clf, message):
    if validate_model(clf):
        vectorizer = HashingVectorizer(decode_error='ignore', ngram_range=(1, 3), 
                                       n_features=2**10, stop_words='english', 
                                       non_negative=True)
        X = vectorizer.transform(message)
        return clf.predict(X)[0]
    else:
        return -1

def update_classifier(clf, batch_X, batch_y, fb_id):
    if validate_model(clf):
        vectorizer = HashingVectorizer(decode_error='ignore', ngram_range=(1, 3), 
                                       n_features=2**10, stop_words='english', 
                                       non_negative=True)
        batch_X = vectorizer.transform(batch_X)
        clf.partial_fit(batch_X, batch_y)
        classifier_dir = "~\\classifiers\\" + fb_id + "\\" + fb_id + ".pkl"
        joblib.dump(clf, expanduser(classifier_dir))

def retrain_classifier(fb_id):
    client = MongoClient()
    db = client.fb_messages
    cursor = db.fb_messages.find()
    corpus = [(x['message'] + ' ' + x['from']['name'], x['class']) for x in cursor]
    X = [x[0] for x in corpus]
    y = [x[1] for x in corpus]
    vectorizer = HashingVectorizer(decode_error='ignore', ngram_range=(1, 3), 
                                       n_features=2**10, stop_words='english', 
                                       non_negative=True)
    X = vectorizer.transform(X)
    clf_parameters = [{'loss': ['hinge', 'log', 'modified_huber'], 
                       'penalty': ['l1', 'l2'],
                       'alpha': [0.00001, 0.0001, 0.001, 0.01],
                       'n_iter': [5, 10, 20]},
                      {'loss': ['hinge', 'squared_hinge'], 
                       'С': [0.5, 0.9, 0.99, 1],
                       'n_iter': [5, 10, 20]},
                      {'penalty': ['l1', 'l2'],
                       'alpha': [0.00001, 0.0001, 0.001, 0.01],
                       'n_iter': [5, 10, 20]}]
                       
    clf_list = [SGDClassifier(), PassiveAggressiveClassifier(), Perceptron()]
    res = 0
    for clf, grid  in zip(clf_list, clf_parameters):
        gs = GridSearchCV(clf, grid, cv= 3, scoring='accuracy')
        gs.fit(X, y)
        score = gs.best_score_
        if score > res:
            model = gs.best_estimator_
            res = score
    classifier_dir = "~\\classifiers\\" + fb_id + "\\" + fb_id + ".pkl"
    joblib.dump(model, expanduser(classifier_dir))